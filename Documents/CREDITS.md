
# Music

* [City Loop](http://opengameart.org/content/city-loop) by [Homingstar](http://opengameart.org/users/homingstar) (CC-BY-SA-3.0)
* [CalmBGM](http://opengameart.org/content/calm-bgm) by [syncopika](http://opengameart.org/users/syncopika) (CC-BY-3.0)

# Graphics

* [Character Templates](http://playerred-1.deviantart.com/art/Pokemon-Character-bases-New-174895108) by [PlayerRed-1](http://playerred-1.deviantart.com/)


## Tilesets from the Tuxemon Project

* [Tuxemon Tileset](http://opengameart.org/content/tuxemon-tileset) by [Buch](http://blog-buch.rhcloud.com) is licensed under [CC BY 3.0](http://creativecommons.org/licenses/by/3.0/)

* [Mike Bramson](mailto:mnbramson@gmail.com)
* [William Edwards](mailto:shadowapex@gmail.com)

* ["Player Sprite"](https://git.tuxemon.org:3000/tuxemon/tuxemon/blob/development/resources/sprites/player_front.png)
by [Mike Bramson](mailto:mnbramson@gmail.com) is licensed under 
[CC BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/)

* ["Electronics Tileset"](https://git.tuxemon.org:3000/tuxemon/tuxemon/blob/development/resources/gfx/tilesets/electronics.png)
by [Mike Bramson](mailto:mnbramson@gmail.com) is licensed under 
[CC BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/)

* ["Floors and Walls Tileset"](https://git.tuxemon.org:3000/tuxemon/tuxemon/blob/development/resources/gfx/tilesets/floorsandwalls.png)
by [Mike Bramson](mailto:mnbramson@gmail.com) is licensed under 
[CC BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/)

* ["Furniture Tileset"](https://git.tuxemon.org:3000/tuxemon/tuxemon/blob/development/resources/gfx/tilesets/furniture.png)
by [Mike Bramson](mailto:mnbramson@gmail.com) is licensed under 
[CC BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/)

* ["Kitchen Tileset"](https://git.tuxemon.org:3000/tuxemon/tuxemon/blob/development/resources/gfx/tilesets/kitchen.png)
by [Mike Bramson](mailto:mnbramson@gmail.com) is licensed under 
[CC BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/)

* ["Plants Tileset"](https://git.tuxemon.org:3000/tuxemon/tuxemon/blob/development/resources/gfx/tilesets/plants.png)
by [Mike Bramson](mailto:mnbramson@gmail.com) is licensed under 
[CC BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/)

* ["Stairs Tileset"](https://git.tuxemon.org:3000/tuxemon/tuxemon/blob/development/resources/gfx/tilesets/stairs.png)
by [Mike Bramson](mailto:mnbramson@gmail.com) is licensed under 
[CC BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/)

* ["Bamboon"](https://git.tuxemon.org:3000/tuxemon/tuxemon/blob/development/resources/gfx/sprites/battle/bamboon-front.png)
by [Mike Bramson](mailto:mnbramson@gmail.com) is licensed under 
[CC BY-SA 4.0](http://creativecommons.org/licenses/by-sa/4.0/)

* ["Kyrodian Legends Overworld Tileset"](http://opengameart.org/content/kyrodian-legends-overworld-props)
by [Midi](http://opengameart.org/users/midi) is licensed under
[CC BY 3.0](http://creativecommons.org/licenses/by/3.0/)

* ["Sign"](https://bitbucket.org/ItsBobberson/tsc)
by [ItsBobberson](https://bitbucket.org/ItsBobberson) is licensed under
[Public Domain](http://creativecommons.org/publicdomain/zero/1.0/)

* ["Sign White"](https://bitbucket.org/ItsBobberson/tsc)
by [ItsBobberson](https://bitbucket.org/ItsBobberson) is licensed under
[Public Domain](http://creativecommons.org/publicdomain/zero/1.0/)

* ["Wood Sign"](https://bitbucket.org/ItsBobberson/tsc)
by [ItsBobberson](https://bitbucket.org/ItsBobberson) is licensed under
[Public Domain](http://creativecommons.org/publicdomain/zero/1.0/)

* ["Sand n Water"](http://tuxemon.weebly.com/artwork.html)
by [luke83](http://tuxemon.weebly.com/) is licensed under
[CC BY 3.0](http://creativecommons.org/licenses/by/3.0/) based on ["Kyrodian Legends Overworld Tileset"](http://opengameart.org/content/kyrodian-legends-overworld-props)
by [Midi](http://opengameart.org/users/midi)

* ["Trainer Sprite Spree"](http://oniwanbashu.deviantart.com/art/Trainer-Sprite-Spree-124465962)
by [Oniwanbashu](http://oniwanbashu.deviantart.com/) is licensed under
[CC BY-NC-SA 3.0](http://creativecommons.org/licenses/by-nc-sa/3.0/)

* ["Calis Overworld Template"](http://minorthreat0987.deviantart.com/art/Calis-Overworld-Template-193004763)
by [Minorthreat0987](http://minorthreat0987.deviantart.com/) and
[Calis Projects](http://www.calisprojects.com/) is licensed under
[CC BY-NC 3.0](http://creativecommons.org/licenses/by-nc/3.0/)

## Tilesets from The Public Pokemon Tileset

[The Public Pokemon Tileset](ttp://thatssowitty.deviantart.com/art/The-Public-Pokemon-Tileset-281342410) is licensed under
[CC-BY-3.0](http://creativecommons.org/licenses/by/3.0/)

This tileset consists of tiles from various artists:

* [Kyle Dove](http://kyle-dove.deviantart.com/)

* [Speedialga](http://speedialga.deviantart.com/)

* Spacemotion

* [Alucus](http://alucus.deviantart.com/)

* [Pokemon Diamond](http://pokemon-diamond.deviantart.com/)

* [Kizemaru Kurunosuke](http://kizemaru-kurunosuke.deviantart.com/)

* [Epicday](http://epicday.deviantart.com/)

* [Thurpok](http://thurpok.deviantart.com/)

* [UltimoSpriter](http://ultimospriter.deviantart.com/)

* [iametrine](http://iametrine.deviantart.com/)

* [minorthreat0987](http://minorthreat0987.deviantart.com/)

* [TyranitarDark](http://tyranitardark.deviantart.com/)

* [Heavy-Metal-Lover](http://heavy-metal-lover.deviantart.com/)

* [KKKaito](http://kkkaito.deviantart.com/)

* [WesleyFG](http://wesleyfg.deviantart.com/)

* [BoOmbxBiG](http://boomxbig.deviantart.com/)

* [EternalTakai](http://eternaltakai.deviantart.com/)

* [Hek-El-Grande](http://hek-el-grande.deviantart.com/)

* [ThatsSoWitty](http://thatssowitty.deviantart.com/)

