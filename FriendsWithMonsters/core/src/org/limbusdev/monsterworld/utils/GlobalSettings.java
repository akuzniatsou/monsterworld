package org.limbusdev.monsterworld.utils;

/**
 * Static container for Settings. Every parameter used in the game should be configured here
 * Created by georg on 21.11.15.
 */
public class GlobalSettings {
    /* ............................................................................ ATTRIBUTES .. */
    public static final int TILE_SIZE = 16;
    public static final int PIXELS_PER_METER = 1;
    public static final int RESOLUTION_X = 800;
    public static final int RESOLUTION_Y = 480;
    public static final int ONE_STEPDURATION_MS = 5;
    public static final int ONE_STEP_DURATION_PERSON = 20;
    public static final int zoom = 2;
    public static final boolean DEBUGGING_ON = false;
    public static final int MONSTER_SPRITES = 22;
    public static final boolean JoyStick = true;
    /* ........................................................................... CONSTRUCTOR .. */
    
    /* ............................................................................... METHODS .. */
    
    /* ..................................................................... GETTERS & SETTERS .. */
}
