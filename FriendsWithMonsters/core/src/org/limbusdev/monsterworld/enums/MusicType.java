package org.limbusdev.monsterworld.enums;

/**
 * Created by georg on 22.11.15.
 */
public enum MusicType {
    TOWN, FOREST, SEA, COAST, CITY, BATTLE, CAVE
}
