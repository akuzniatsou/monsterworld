package org.limbusdev.monsterworld.enums;

/**
 * Created by georg on 24.01.16.
 */
public enum Element {
    EARTH, FIRE, WATER, AIR, LIGHT
}
