package org.limbusdev.monsterworld.enums;

/**
 * SkyDirection can be used to create pathes in
 * {@link org.limbusdev.monsterworld.ecs.components.PathComponent}
 * Created by georg on 22.11.15.
 */
public enum SkyDirection {
    N, NE, NW, NSTOP,
    W, WSTOP,
    E, ESTOP,
    S, SW, SE, SSTOP,
}
