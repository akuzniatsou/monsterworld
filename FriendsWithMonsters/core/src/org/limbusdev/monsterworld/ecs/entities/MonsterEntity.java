package org.limbusdev.monsterworld.ecs.entities;

import com.badlogic.ashley.core.Entity;

/**
 * Created by georg on 06.12.15.
 */
public class MonsterEntity extends Entity {
    /* ............................................................................ ATTRIBUTES .. */

    /* ........................................................................... CONSTRUCTOR .. */
    
    /* ............................................................................... METHODS .. */
    
    /* ..................................................................... GETTERS & SETTERS .. */
}
