package org.limbusdev.monsterworld.geometry;

/**
 * Created by georg on 25.11.15.
 */
public class IntVector2 {
    /* ............................................................................ ATTRIBUTES .. */
    public int x,y;
    /* ........................................................................... CONSTRUCTOR .. */

    public IntVector2(int x, int y) {
        this.x = x;
        this.y = y;
    }
    
    /* ............................................................................... METHODS .. */
    
    /* ..................................................................... GETTERS & SETTERS .. */
}
